# Module shoul

Simple asserts for testing

```
import shoul;

10.should == 10;
10.should.not == 11;

10.should in [10, 11, 12];
10.should.not in [11, 12, 13];
10.should in [10: 11, 12: 13];
"Hello".should in "Hello World";

10.should.less = 11; // 10 < 11
10.should.not.more = 10; // 10 <= 10

10.should.more = 9; // 10 > 9
10.should.not.less = 10; // 10 >=10
10.should.not.less(10); // 10 >=10
```

