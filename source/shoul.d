module shoul;

import std.conv;
import std.stdio;

public auto should(T)(T v)
{
	return Should!T(v);
}

private struct Should(T)
{
	T actual;
	bool negative;
	this(T actual)
	{
		this.actual = actual;
	}

	void opEquals(R)(const R other) const
	{
		assert(negative ^ (other == actual), text("expected - " ~ (negative ? "not " : ""), other, ", but got - ", actual));
	}

	auto not()
	{
		this.negative = !this.negative;
		return this;
	}

	void opBinary(string op, R)(const R rhs) const
	{
		static if (op == "in")
		{
			import std.algorithm : canFind;

			static if (__traits(compiles, (actual in rhs)))
			{
				assert(negative ^ ((actual in rhs) != null), text("expected - ", actual, " in ", rhs));
			}
			else
			{
				assert(negative ^ canFind(rhs, actual), text("expected - ", actual, " in ", rhs));
			}
		}
		else
			static assert(0, "not supported op");
	}

	void less(R)(R rhs)
	{
		assert(negative ^ (actual < rhs), text("expected - ", actual, " < ", rhs));
	}

	void more(R)(R rhs)
	{
		assert(negative ^ (actual > rhs), text("expected - ", actual, " > ", rhs));
	}
}

@("Equal") @should
unittest
{
	10.should == 10;
	10.should.not == 11;
	try
		10.should.not == 10;
	catch (Throwable e)
	{
	}
}

@("In") @should
unittest
{
	10.should in [10, 11, 12];
	10.should.not in [11, 12, 13];
	try
		10.should.not in [10, 11, 12];
	catch (Throwable e)
	{
	}
	10.should in [10: 11, 12: 13];
	"Hello".should in "Hello World";
}

@("Less") @should
unittest
{
	10.should.less = 11;
	10.should.not.less = 10;
	10.should.not.less = 9;
}

@("more") @should
unittest
{
	10.should.more = 9;
	10.should.not.more = 10;
	10.should.not.more = 11;
}
